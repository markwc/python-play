def hello(name):
  print(f'It is good to meet you, {name}!')

def age_response(age):
  if age > 50:
    print(f'Hello, old person!')
  elif age < 18:
    print(f'Hello, kiddo!')
  else:
    print(f'Hello!')

print(f'What is your name?')
myName = input()
hello(myName)
print(f'What is your age?')
myAge = input()
age_response(int(myAge))
