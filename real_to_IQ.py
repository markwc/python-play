import numba as nb
import numpy as np
import scipy as sp
from numba import jit
# import tensorflow as tf
import torch
import torch.fft as fft

import matplotlib.pyplot as plt

def save_to_real_file(file_name):
    print(f'save_to_real_file')
    num_symbols = 1024
    x_symbols = np.random.randint(0, 2, num_symbols)*2-1 # -1 and 1's
    n = np.random.randn(num_symbols) # AWGN with unity power
    r = x_symbols + n * np.sqrt(0.01) # noise power of 0.01
    print(r)
    plt.plot(np.real(r), np.imag(r), '.')
    plt.grid(True)
    plt.show()
    r = r.astype(np.float64)
    r.tofile(file_name)

def convert_to_IQ(input_file, output_file):
    print(f'convert_to_IQ')
    samples = np.fromfile(input_file, np.float64)
    print(samples)
    plt.plot(np.real(samples), np.imag(samples), '.')
    plt.grid(True)
    plt.show()
    # X = fft.fft(tf.convert_to_tensor(samples))

def main():
    print(f'MAIN')
    save_to_real_file('bpsk_in_noise.r')
    convert_to_IQ('bpsk_in_noise.r', 'bpsk_in_noise.iq')

if __name__ == '__main__':
    main()